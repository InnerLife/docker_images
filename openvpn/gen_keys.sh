#!/bin/sh
cd /root/easyrsa
./easyrsa init-pki
./easyrsa build-ca nopass
./easyrsa gen-req server nopass
./easyrsa sign-req server server
./easyrsa gen-dh
openvpn --genkey --secret pki/ta.key
cp pki/ca.crt /etc/openvpn/ca.crt
cp pki/private/server.key /etc/openvpn/server.key
cp pki/issued/server.crt /etc/openvpn/server.crt
cp pki/dh.pem /etc/openvpn/dh.pem
cp pki/ta.key /etc/openvpn/ta.key
