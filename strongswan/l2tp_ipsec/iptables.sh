#!/bin/sh
iptables -t nat -A POSTROUTING -s 10.8.1.0/24 -o eth0 -j MASQUERADE
ip6tables -t nat -A POSTROUTING -s fd00:0000:0000:1249::/64 -o eth0 -j MASQUERADE
